import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;

public class JavaScreenShot {

	private static int intervalInSeconds = 15;

	public static void main(String[] args) {
		
		if( args.length > 0 ) {
			intervalInSeconds = Integer.decode(args[0]);
		}
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle screen = new Rectangle( 0, 0, screenSize.width, screenSize.height );
		Robot robot;
		try {
			robot = new Robot();
		} catch( AWTException e ) {
			System.err.println( "Could not acquire robot for screen capture" );
			return;
		}
		
		System.out.println( "Screen shots will be saved every " + intervalInSeconds + " seconds" );
		
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HHmmss" );
		while( true ) {
			String filename = sdf.format( new Date() ) + ".png";
			BufferedImage image = robot.createScreenCapture( screen );
			File capturedScreenshotFile = new File( filename );
			try {
				ImageIO.write( image, "png", capturedScreenshotFile );
			} catch( IOException e ) {
				System.err.println( "I/O error writing screen shot file: " + filename );
			}
			
			System.out.println( "Screenshot saved: " + filename );
			
			try {
				Thread.sleep( intervalInSeconds * 1000 );
			} catch( InterruptedException e ) {
				System.err.println( "Sleep was interrupted" );
			}
		}
	}
	
}
