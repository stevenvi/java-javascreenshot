# README #

This program takes a screen shot of the desktop every 15 seconds.

It had a very specific use-case, in that I would be performing with a band and it was live streamed over the Internet. I could start up the stream, run this program, then go and perform. When I was done, I could see snippets of the performance.

(The actual use for it was that I would create an "album" of the performance, and use the best image from this set of screen shots as the album art.)

It's likely that this is of little use to anyone.

# Running #
Compile with Java (not sure the minimum version requirements, probably 6), and execute the single class.